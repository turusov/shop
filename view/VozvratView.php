<?PHP

/**
 * Simpla CMS
 *
 * @copyright 	2011 Denis Pikusov
 * @link 		http://simp.la
 * @author 		Denis Pikusov
 *
 * Отображение статей на сайте
 * Этот класс использует шаблоны articles.tpl и article.tpl
 *
 */
require_once('View.php');

class VozvratView extends View
{
	function fetch()
	{   
	    
		if($this->request->method('post'))
		{		
		    $vozvrat = new stdClass;
			$vozvrat->name            = $this->request->post('name');
            $vozvrat->email           = $this->request->post('email');
            $vozvrat->phone           = $this->request->post('phone');
            $vozvrat->name_tovar      = $this->request->post('name_tovar');         
            $vozvrat->invoice           = $this->request->post('invoice');
            $vozvrat->reason          = $this->request->post('reason');
            $vozvrat->message         = $this->request->post('message');
            
            //Тип ошибки reason
            switch ($vozvrat->reason)
            {
                case 1:
                    $vozvrat->reason = "Получен/доставлен неисправным (сломанным)";
                break;
                case 2:
                    $vozvrat->reason = "Ошибочный товар";
                break;
                case 3:
                    $vozvrat->reason = "Другое";
                break;
            }
            $this->design->assign('name', $vozvrat->name);
            $this->design->assign('email', $vozvrat->region);
            $this->design->assign('phone', $vozvrat->city);
            $this->design->assign('name_tovar', $vozvrat->email);
            $this->design->assign('invoice', $vozvrat->organization);
            $this->design->assign('reason', $vozvrat->phone);
            $this->design->assign('message', $vozvrat->message );
            
            $email_template = $this->design->fetch($this->config->root_dir.'design/'.$this->settings->theme.'/html/email_vozvrat.tpl');
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            require_once('api/Simpla.php');
            $simpla = new Simpla();   
            $email = $simpla->settings->notify_from_email;               
    		$subject = "Заявка на возврат товара";
            mail($email, $subject, $email_template, $headers);
            $this->design->assign('form', "access");
        }
        
        

	

		$body = $this->design->fetch('vozvrat.tpl');
		
		return $body;
	}
}
