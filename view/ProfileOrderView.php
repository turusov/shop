<?PHP

require_once('View.php');

class ProfileOrderView extends View
{
	function fetch()
	{
	   $error = 0;
      
	   $id_user = $_SESSION['user_id'];
       $user_info = $this->orders->get_user($id_user);     

       if (!empty($user_info->email))
       { 
           $orders = $this->users->get_orders($user_info->email);
           $count_order = count($orders);
           if ($count_order == 1)
           {
               $id = $orders[0]->id;
               $products= $this->orders->orders_product($id, $count_order); 
           }
           else
           {
                for ($i = 0; $i < $count_order; $i++) 
                {
                    $id = $orders[$i]->id;
                    $temp =  $this->orders->orders_product($id, $count_order);
                    $temp = $temp;
                    $products[$i] = $temp;
                }
           } 
           $products = $this->orders->normal_array($products);
           $temp = $products[0];
           if (is_array($temp)) 
           {   
             $products = $this->orders->normal_array($products);
           }
           

           $this->design->assign('count_order', $count_order);
           $this->design->assign('products', $products);
           $this->design->assign('orders', $orders);
       }
	   return $this->design->fetch('orders.tpl');
	}	
}
