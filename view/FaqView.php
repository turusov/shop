<?PHP

/**
 * Simpla CMS
 *
 * @copyright 	2011 Denis Pikusov
 * @link 		http://simp.la
 * @author 		Denis Pikusov
 *
 * Отображение статей на сайте
 * Этот класс использует шаблоны articles.tpl и article.tpl
 *
 */
require_once('View.php');

class FaqView extends View
{
	function fetch()
	{   
	    if($this->request->method('post'))
	    {
            $qwestion = new stdClass;
			$qwestion->name    = $this->request->post('name');
            $qwestion->email   = $this->request->post('email');
            $qwestion->text    = $this->request->post('text');
            $this->faq->add_qwestion($qwestion);
             $this->design->assign('acess', 1); 
        }
        $faq = $this->faq->get_list();
        $this->design->assign('qwestion', $qwestion); 
        $this->design->assign('faqs', $faq);
		$body = $this->design->fetch('faq.tpl');
		return $body;
	}
}
