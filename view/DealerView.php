<?PHP

/**
 * Simpla CMS
 *
 * @copyright 	2011 Denis Pikusov
 * @link 		http://simp.la
 * @author 		Denis Pikusov
 *
 * Отображение статей на сайте
 * Этот класс использует шаблоны articles.tpl и article.tpl
 *
 */
require_once('View.php');

class DealerView extends View
{
	function fetch()
	{   
	    
		if($this->request->method('post'))
		{		
		    $dealer = new stdClass;
			$dealer->name            = $this->request->post('name');
            $dealer->region          = $this->request->post('region');
            $dealer->city            = $this->request->post('city');
            $dealer->email           = $this->request->post('email');
            $dealer->organization    = $this->request->post('organization');
            $dealer->phone           = $this->request->post('phone');
            $dealer->message         = $this->request->post('message');
		    //Отправка email                  
            $temp =  $this->users->get_region($dealer->region);
            $dealer->region = $temp ->title_ru;
            $temp = $this->users->get_city($dealer->city);
            $dealer->city = $temp ->title_ru;
            $this->design->assign('name', $dealer->name);
            $this->design->assign('region', $dealer->region);
            $this->design->assign('city', $dealer->city);
            $this->design->assign('email', $dealer->email);
            $this->design->assign('organization', $dealer->organization);
            $this->design->assign('phone', $dealer->phone);
            $this->design->assign('message', $dealer->message );
            $email_template = $this->design->fetch($this->config->root_dir.'design/'.$this->settings->theme.'/html/email_dealer.tpl');
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            require_once('api/Simpla.php');
            $simpla = new Simpla();   
            $email = $simpla->settings->notify_from_email;               
    		$subject = "Заявка на сотрудничество";
            mail($email, $subject, $email_template, $headers);
            $this->design->assign('form', "access");
        }
        
        

	

		$body = $this->design->fetch('dealer.tpl');
		
		return $body;
	}
}
