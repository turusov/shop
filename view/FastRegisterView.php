<?PHP

require_once('View.php');

class FastRegisterView extends View
{
	function fetch()
	{
		$default_status = 1; // Активен ли пользователь сразу после регистрации (0 или 1)
		if($this->request->method('post') && $this->request->post('register'))
		{
		    $this->design->assign('email', $email);
			if (!$this->request->post('email'))
            $this->design->assign('error', 'empty_email');
			$email = $this->request->post('email');
            $name = substr($email, 0, strripos($email, "@"));
			$password = substr(md5(mt_rand()), 0, 6);
			$captcha_code = $this->request->post('captcha_code');
            $this->db->query('SELECT count(*) as count FROM __users WHERE email=?', $email);
			$user_exists = $this->db->result('count');
            if ($user_exists == 1)
                $this->design->assign('error', 'user_exists');
			if($user_id = $this->users->add_user(array('name'=>$name, 'email'=>$email, 'password'=>$password, 'enabled'=>$default_status, 'last_ip'=>$_SERVER['REMOTE_ADDR'])))
			{
			    $this->design->assign('user', $user);
    			$this->design->assign('email', $email);
                $this->design->assign('name', $name);
                $this->design->assign('password', $password);
    			// Отправляем письмо
    			$email_template = $this->design->fetch($this->config->root_dir.'design/'.$this->settings->theme.'/html/send_reg.tpl');
    			$subject = "Регистрация нового пользователя";
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                mail($email, $subject, $email_template, $headers);
                $this->design->smarty->clearAssign('user');
    			$this->design->smarty->clearAssign('email');
                $this->design->smarty->clearAssign('name');
                $this->design->smarty->clearAssign('password');
   				$_SESSION['user_id'] = $user_id;
				if(!empty($_SESSION['last_visited_page']))
					header('Location: '.$_SESSION['last_visited_page']);				
				else
					header('Location: '.$this->config->root_url);
			}
			else
				$this->design->assign('error', 'unknown error');
	
		}
		return $this->design->fetch('fast_register.tpl');
	}	
}
