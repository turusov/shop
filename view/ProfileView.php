<?PHP

require_once('View.php');

class ProfileView extends View
{
	function fetch()
	{
       $error = 0;
        
	   $id_user = $_SESSION['user_id'];
       if (!empty($id_user))
       {
           $user_info = $this->orders->get_user($id_user);
       }
	   if (!empty($_POST))
       {
        	$user             = new stdClass;
        	$user->name       = $this->request->post('name');
        	$user->email      = $this->request->post('email');
        	$user->phone      = $this->request->post('phone');
            $user->password   = $this->request->post('password');
            $password2        = $this->request->post('password2');
            $user->country    =  1;
            $user->region     = $this->request->post('region');
            $user->city       = $this->request->post('city');
            $user->adress     = $this->request->post('adress');
            
            if ($user_info->email != $user->email)
            {
                if ($this->users->check_email($user->email) != 0)
                {
                    $this->design->assign('error', 'incoret_email');
                    $error = 1;
                }
            }
        

            if ($error != 1)
            {
                if ($user->password == $password2)
                {
                    if (!$this->users->check_password($user->email, $user->password) && $user->password != "Пароль")
                    {
                        $this->users->update_user($id_user, $user);
                        $user_info = $this->users->get_user($id_user);
                        $this->design->assign('update', 'true');
                    }
                    else
                    {
                        unset($user->password);
                        $this->users->update_user($id_user, $user);
                        $user_info = $this->users->get_user($id_user);
                        $this->design->assign('update', 'true');
                        
                    }
                        
                }
                else
                {
                    $this->design->assign('error', 'different_passwords');
                }
            }
       }
       if (isset($user_info->region))
       {
        $temp = $this->users->get_region($user_info->region);
        $temp = $temp->title_ru;
        $user_info->name_reg = $temp;
       }
       if (isset($user_info->region))
       {
        $temp = $this->users->get_city($user_info->city);
        $temp = $temp->title_ru;
        $user_info->name_city = $temp;  
       }
        
        //exit;
        $this->design->assign('user_info', $user_info);
		return $this->design->fetch('profile_contact.tpl');
	}	
}
