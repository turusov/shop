<?PHP

/**
 * Simpla CMS
 *
 * @copyright 	2011 Denis Pikusov
 * @link 		http://simp.la
 * @author 		Denis Pikusov
 *
 * Отображение статей на сайте
 * Этот класс использует шаблоны articles.tpl и article.tpl
 *
 */
require_once('View.php');

class DeliveryView extends View
{
	function fetch()
	{   
		$body = $this->design->fetch('delivery.tpl');	
		return $body;
	}
}
