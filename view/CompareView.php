<?PHP

/**
 * Simpla CMS
 *
 * @copyright   2013 Redline Studio
 * @link     http://simplashop.com
 * @author     Artiom Mitrofanov
 *
 */

require_once('View.php');


class CompareView extends View
{

    public function __construct()
    {
        parent::__construct();
 
    }

	//////////////////////////////////////////
	// Основная функция
	//////////////////////////////////////////
	function fetch()
	{
        $limit = 4;
        
        $id = $this->request->get('id', 'integer');
        
        $products_ids = (array)$_SESSION['compared_products'];
            
        if($this->request->get('action', 'string') == 'delete') {
            $key = array_search($id, $products_ids);
            unset($products_ids[$key]);    
        }   
        elseif($id > 0) {
            array_push($products_ids, $id);
            $products_ids = array_unique($products_ids);        
        }

        $products_ids = array_slice($products_ids, 0, $limit);
        $products_ids = array_reverse($products_ids);
        
        if(!count($products_ids))
            unset($_SESSION['compared_products']);
        else
            $_SESSION['compared_products'] = $products_ids;  
              
        $products = array();
        
        if(count($products_ids)) {
            
            foreach($this->products->get_products(array('id'=>$products_ids)) as $p)
                $products[$p->id] = $p;
            
            foreach($this->products->get_images(array('product_id'=>$products_ids)) as $image)
            if(isset($products[$image->product_id]))
                $products[$image->product_id]->images[] = $image;
            
            foreach($this->variants->get_variants(array('product_id'=>$products_ids, 'in_stock'=>true)) as $variant)
            if(isset($products[$variant->product_id]))
                $products[$variant->product_id]->variants[] = $variant;
                
            $features = array();
            foreach($this->features->get_product_options($products_ids) as $feature) {
              $features[$feature->feature_id] = $feature->name;
              
              if(isset($products[$feature->product_id]))
                  $products[$feature->product_id]->features[$feature->feature_id] = $feature;
            }
          
            foreach($products_ids as $id)
            {  
                if(isset($products[$id]))
                {
                    if(isset($products[$id]->images[0]))
                        $products[$id]->image = $products[$id]->images[0];
                        
                    if(isset($product->variants[0]))
                        $product->variant = $product->variants[0];
                }
            }
            
        }

        // Содержимое сравнения товаров
        $this->design->assign('products', $products);
        //Свойства для сравнения
        $this->design->assign('features', $features);

        // Выводим шаблона
//        print $this->design->fetch('compare.tpl');
//        exit;
        return $this->design->fetch('compare.tpl');

	}

}
