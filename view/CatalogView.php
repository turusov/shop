<?PHP

require_once('View.php');

class CatalogView extends View
{
    function fetch()
	{
	    // Страницы
		$pages = $this->pages->get_pages(array('visible'=>1));		
		$this->design->assign('pages', $pages);	
	   	$body = $this->design->fetch('catalog.tpl');
		return $body;
    }
}