<?PHP

/**
 * Simpla CMS
 *
 * @copyright 	2011 Denis Pikusov
 * @link 		http://simp.la
 * @author 		Denis Pikusov
 *
 * Этот класс использует шаблон index.tpl,
 * который содержит всю страницу кроме центрального блока
 * По get-параметру module мы определяем что сожержится в центральном блоке
 *
 */

require_once('View.php');

class IndexView extends View
{	
	public $modules_dir = 'view/';

	public function __construct()
	{
		parent::__construct();
	}

		
	/**
	 *
	 * Отображение
	 *
	 */
	function fetch()
	{

	   if($this->request->method('post') && $_POST['type'] == "fast_registration" && isset($_POST['email']))
		{

		    $default_status = 1;  
		    $this->design->assign('email', $email);
			if (!$this->request->post('email'))
            $this->design->assign('error', 'empty_email');
			$email = $this->request->post('email');
            $name = substr($email, 0, strripos($email, "@"));
			$password = substr(md5(mt_rand()), 0, 6);	 
            $this->db->query('SELECT count(*) as count FROM __users WHERE email=?', $email);
			
            $user_exists = $this->db->result('count');
            if ($user_exists == 1)
                $this->design->assign('error', 'user_exists');
            else
			if($user_id = $this->users->add_user(array('name'=>$name, 'email'=>$email, 'password'=>$password, 'enabled'=>$default_status, 'last_ip'=>$_SERVER['REMOTE_ADDR'])))
			{
			    $this->design->assign('user', $user);
    			$this->design->assign('email', $email);
                $this->design->assign('name', $name);
                $this->design->assign('password', $password);
    			// Отправляем письмо
    			$email_template = $this->design->fetch($this->config->root_dir.'design/'.$this->settings->theme.'/html/send_reg.tpl');
    			$subject = "Регистрация нового пользователя";
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                mail($email, $subject, $email_template, $headers);
                $this->design->smarty->clearAssign('user');
    			$this->design->smarty->clearAssign('email');
                $this->design->smarty->clearAssign('name');
                $this->design->smarty->clearAssign('password');
                $_SESSION['user_id'] = $user_id;
			      $this->design->assign('access', 'user_exists');
                if(!empty($_SESSION['last_visited_page']))
					header('Location: '.$_SESSION['last_visited_page']);				
				else
                {
                     setcookie('reg', 1);
                     header('Location: /user/profile');
                }
					
			}  
		}
			// Содержимое корзины
		$this->design->assign('cart',		$this->cart->get_cart());
	   
        // Категории товаров
		$this->design->assign('categories', $this->categories->get_categories_tree());

	    // Категории статей
		$this->design->assign('articles_categories', $this->articles_categories->get_articles_categories_tree());

        //Сравнение
        $compared = (array)$_SESSION['compared_products'];
        $this->design->assign('compared_products', ($compared[0] > 0) ? $compared : array());

		// Страницы
		$pages = $this->pages->get_pages(array('visible'=>1));		
		$this->design->assign('pages', $pages);				
		
        // Текущий модуль (для отображения центрального блока)
		
        // Отбражение категорий и товаров
        $module = $this->request->get('module', 'string');     
        $url = $this->request->get('page_url', 'string');
       
        if (!$module) {
        if($_SERVER['REQUEST_URI'] == "/") {
            $module = "MainView";
        } elseif($url == "shares") {
            $module = "BlogView";
        } elseif($_SERVER['REQUEST_URI'] == "/dostavka") {
            require_once('DeliveryView.php');
            $objectBlog = new DeliveryView();
            $module = "DeliveryView";
        } else {
            require_once('PageView.php');
            $objectPage = new PageView();
            $page = $objectPage->pages->get_page($url);
            if (!empty($page) || $page->visible) {
                $module = "PageView";
            }
            require_once('ProductsView.php');
            $objectCategory = new ProductsView();
            $objectBrand = new ProductsView();
            $category = $objectCategory->categories->get_category((string)$url);
            $brand = $objectBrand->brands->get_brand((string)$url);
            if ((!empty($category) && $category->visible) || !empty($brand)) {
                $module = "ProductsView";
            }
            require_once('ProductView.php');
            $objectProduct = new ProductView();
            $product = $objectProduct->products->get_product((string)$url);
            if (!empty($product) && $product->visible) {
                $module = "ProductView";
            }
        }



        }





        $module = preg_replace("/[^A-Za-z0-9]+/", "", $module);


		// Если не задан - берем из настроек
		if(empty($module))
			return false;
		//$module = $this->settings->main_module;

		// Создаем соответствующий класс
		if (is_file($this->modules_dir."$module.php"))
		{

				include_once($this->modules_dir."$module.php");
				if (class_exists($module))
				{
					$this->main = new $module($this);
				} else return false;
		} else return false;

		// Создаем основной блок страницы
		if (!$content = $this->main->fetch())
		{

			return false;
		}		

		// Передаем основной блок в шаблон
		$this->design->assign('content', $content);		
		
		// Передаем название модуля в шаблон, это может пригодиться
		$this->design->assign('module', $module);
				
		// Создаем текущую обертку сайта (обычно index.tpl)
		$wrapper = $this->design->smarty->getTemplateVars('wrapper');
		if(is_null($wrapper))
			$wrapper = 'index.tpl';

			
		if(!empty($wrapper))
			return $this->body = $this->design->fetch($wrapper);
		else
			return $this->body = $content;

	}
}
