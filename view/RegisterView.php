<?PHP

require_once('View.php');

class RegisterView extends View
{
	function fetch()
	{
		$default_status = 0; // Активен ли пользователь сразу после регистрации (0 или 1)
		
		if($this->request->method('post') && $this->request->post('register'))
		{
		  
			$name = $this->request->post('name');
			$email = $this->request->post('email');
			$password = $this->request->post('password');
			$captcha_code = $this->request->post('captcha_code');
			
			$this->design->assign('name', $name);
			$this->design->assign('email', $email);
			
			$this->db->query('SELECT count(*) as count FROM __users WHERE email=?', $email);
			$user_exists = $this->db->result('count');
            
           
			if($user_exists)
				$this->design->assign('error', 'user_exists');
			elseif(empty($name))
				$this->design->assign('error', 'empty_name');
			elseif(empty($email))
				$this->design->assign('error', 'empty_email');
			elseif(empty($password))
				$this->design->assign('error', 'empty_password');		
			elseif(empty($_SESSION['captcha_code']) || $_SESSION['captcha_code'] != $captcha_code || empty($captcha_code))
			{
				$this->design->assign('error', 'captcha');
			}
			elseif($user_id = $this->users->add_user(array('name'=>$name, 'email'=>$email, 'password'=>$password, 'enabled'=>1, 'last_ip'=>$_SERVER['REMOTE_ADDR'])))
			{
			
				$_SESSION['user_id'] = $user_id;
    			$this->design->assign('email', $email);
                $this->design->assign('name', $name);
                $this->design->assign('password', $password);
    			// Отправляем письмо
    			$email_template = "<html>
                	<body>
                		<p>На сайте <b>$settings->site_name</b> был сделан запрос на регистрацую нового пользователя.</p>
                		<p>Ваши учетные данные</p>
                		<p>Логин: $name</p>
                		<p>E-mail: $email</p>
                		<p>Пароль: $password</p>
                	</body>
                </html>";
                $subject = "Регистрация";
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                mail($email, $subject, $email_template, $headers);

			}
			else
				$this->design->assign('error', 'unknown error');
	
		}
		return $this->design->fetch('register.tpl');
	}	
}
