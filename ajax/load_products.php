<?
    require_once('../api/Simpla.php');
    $simpla = new Simpla();
    $page_url = $simpla->request->post('page_url');
    $page_url = str_replace ("/", "", $page_url);
    //$page_url = 'ekzotika';
    $count_now = $simpla->request->post('count_now');
    //$count_now = 20;
    $count_page_products = $simpla->settings->products_num;
    $template = "";
    $data = array();
    $products = array();
	$filter = array();
	$filter['visible'] = 1;	
    if($sort = $simpla->request->get('sort', 'string'))
		$_SESSION['sort'] = $sort;		
	if (!empty($_SESSION['sort']))
		$filter['sort'] = $_SESSION['sort'];			
	else
		$filter['sort'] = 'name';			
	$simpla->design->assign('sort', $filter['sort']);
		

    
    $category = $simpla->categories->get_category((string)$page_url);
    $filter['category_id'] = $category->children;

    //???????? ?? ??????? ????? ???????
    $page = $count_now / $count_page_products;
    $page = $page + 1;
	//?????????? ???????
      
//	$filter['category_id'] = $category->id;
    $products_count = $simpla->products->count_products($filter);
	
    
    $pages_num = ceil($products_count/$count_page_products);



    if ($pages_num >= $page) {       
        
        $filter['page'] = $page;
    	$filter['limit'] = $count_page_products;
    
    	// ?????? 
    	
    	foreach($simpla->products->get_products($filter) as $p)
    		$products[$p->id] = $p;
       	if(!empty($products))
    	{
    		$products_ids = array_keys($products);
    		foreach($products as &$product)
    		{
    			$product->variants = array();
    			$product->images = array();
    			$product->properties = array();
    		}
            
            $features = $simpla->features->get_product_options($products_ids);
            foreach($features as &$feature)
            {
                $products[$feature->product_id]->features[] = $feature;
            }
    
    		$variants = $simpla->variants->get_variants(array('product_id'=>$products_ids, 'in_stock'=>true));
    		foreach ($variants as $key=>$variant) {
    		  $variants[$key]->price = number_format($variants[$key]->price, 0, ',', ' ');
    		}
          
            //number_format($number, 2, ',', ' ');
    		foreach($variants as &$variant)
    		{
    			//$variant->price *= (100-$discount)/100;
    			$products[$variant->product_id]->variants[] = $variant;
    		}
    
    		$images = $simpla->products->get_images(array('product_id'=>$products_ids));
    		foreach($images as $image)
    			$products[$image->product_id]->images[] = $image;
    
    		foreach($products as &$product)
    		{
    			if(isset($product->variants[0]))
    				$product->variant = $product->variants[0];
    			if(isset($product->images[0]))
    				$product->image = $product->images[0];
    		}
            
            
    	}         
    }
    
    if ($pages_num < $page+1) {
        $data['end_page'] = 1;
    }

    $simpla->design->assign('products', $products);
    $data['template'] = $simpla->design->fetch($simpla->config->root_dir.'design/'.$simpla->settings->theme.'/html/ajax_products.tpl');
	
    header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");		
	print json_encode($data);

 