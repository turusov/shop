<?php
    error_reporting(E_ALL ^ E_NOTICE);
    chdir('..');
    require_once('api/Simpla.php');
    $simpla = new Simpla();
    
    session_start();
    
    $limit = 3;
    
    $id = $simpla->request->get('id', 'integer');
    
    $products_ids = (array)$_SESSION['compared_products'];

    if($simpla->request->get('action', 'string') == 'delete') {
        $key = array_search($id, $products_ids);
        unset($products_ids[$key]);    
    }   
    else {
        array_push($products_ids, $id);
        $products_ids = array_unique($products_ids);        
    }

    $products_ids = array_slice($products_ids, 0, $limit);
    $products_ids = array_reverse($products_ids);




    if(!count($products_ids))
        unset($_SESSION['compared_products']);
    else
        $_SESSION['compared_products'] = $products_ids;

    $simpla->design->assign('compared_products', $products_ids);
    
    header("Content-type: text/html; charset=UTF-8");
    header("Cache-Control: must-revalidate");
    header("Pragma: no-cache");
    header("Expires: -1");        
    print $simpla->design->fetch('compare_informer.tpl');
