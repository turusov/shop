<?
    require_once('../api/Simpla.php');
    $simpla = new Simpla();
    $product_id = $simpla->request->post('product_id', 'integer');
   

    $product = array();
    $product = $simpla->products->get_product($product_id);
   
	$product->images = $simpla->products->get_images(array('product_id'=>$product->id));
	$product->image = reset($product->images);
	$variants = array();
	foreach($simpla->variants->get_variants(array('product_id'=>$product->id, 'in_stock'=>true)) as $v)
		$variants[$v->id] = $v;
	
	$product->variants = $variants;
	
	// Вариант по умолчанию
	if(($v_id = $simpla->request->get('variant', 'integer'))>0 && isset($variants[$v_id]))
		$product->variant = $variants[$v_id];
	else
		$product->variant = reset($variants);
    	
 	$product->variant->price =  number_format($product->variant->price, 0, ',', ' ');
    			
	$product->features = $simpla->features->get_product_options(array('product_id'=>$product->id));

   
    $simpla->design->assign('product', $product);
    $data = $simpla->design->fetch($simpla->config->root_dir.'design/'.$simpla->settings->theme.'/html/product_popup.tpl');
	
    
    
    header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");		
	print json_encode($data);

 