<?
    require_once('../api/Simpla.php');
    $simpla = new Simpla();
    $category_url = "unichtozhiteli-gryzunov";
    if (!empty($category_url))
	{
		$category = $this->categories->get_category((string)$category_url);
		if (empty($category) || (!$category->visible && empty($_SESSION['admin'])))
			return false;
		$this->design->assign('category', $category);
		$filter['category_id'] = $category->children;
	}