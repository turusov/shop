<?php
	 require_once('../api/Simpla.php');
    $simpla = new Simpla();
    $type = $simpla->request->POST('type', 'string');
    $value = $simpla->request->POST('value', 'string');
    switch ($type) {
    case "Region":
        $simpla->db->query("SELECT region_id, title_ru FROM s_regions WHERE country_id = 1 ORDER BY title_ru");
        break;
    case "City":
        $simpla->db->query("SELECT `city_id`, `title_ru` FROM `s_cities` WHERE `region_id`=? ORDER BY title_ru", $value);
        break;
    }
	$products = $simpla->db->results();
    print json_encode($products);
 
