<?php
    require_once('../api/Simpla.php');
    session_start();
    $simpla = new Simpla();
    
    $options = $simpla->request->POST('option');
    $product = $simpla->request->POST('product', 'int');
    
    $temp = 0;
    $charact_set = "";
    foreach ($options as $option){
        if ($option != "")
        {
            $query = $simpla->db->placehold('SELECT add_price, add_percent,id_characteristics FROM s_product_value WHERE id_product = ? AND id_characteristics_value = ?', $product, $option);

            $simpla->db->query($query);
            $array = $simpla->db->result();
            
            foreach($simpla->variants->get_variants(array('product_id'=>$product, 'in_stock'=>true)) as $v)
			    $variants[] = $v;
            
            
            if ($array->add_percent) {
                $temp = $temp + ($variants[0]->price / 100) * $array->add_percent;
            }
            else {
                $temp = $temp +  $array->add_price;
            }

            $id_characteristics =  $array->id_characteristics; 
    
            $query = $simpla->db->placehold('SELECT name FROM s_characteristics WHERE Id = ?', $id_characteristics);
            $simpla->db->query($query);
            $str = $simpla->db->result('name');
            
            $query = $simpla->db->placehold('SELECT name FROM s_characteristics_value WHERE Id = ?', $option);
            $simpla->db->query($query);
            $str2 = $simpla->db->result('name');
         
            $charact_set = "$charact_set $str - $str2 <br/>";
           
        }
    }

    

   
    
    $variants[0]->price + $temp;
    
    
    $end_price = (int)$variants[0]->price + (int)$temp;
    
    $end_price = number_format($end_price, 0, ',', ' ');
	
    
    $_SESSION['new_price'][$product] = $temp;
    $_SESSION['product_options'][$product] = $charact_set;
    print json_encode($end_price);