<?
require_once('Simpla.php');

class Faq extends Simpla
{
    public function get_all()
	{
	   	$query = $this->db->placehold("SELECT * FROM __faq");
        $this->db->query($query);
		return $this->db->results();
    }
    public function get_list()
	{
	   	$query = $this->db->placehold("SELECT * FROM __faq WHERE status = 1 ORDER BY  orders ASC");
        $this->db->query($query);
		return $this->db->results();
    }

    public function get_count()
	{
        $query = $this->db->placehold("SELECT count(distinct id) as count 
                                        FROM s_faq");
		$this->db->query($query);	
		return $this->db->result('count');
    }
    public function get_faq($id)
	{
        $query = $this->db->placehold("SELECT * FROM __faq WHERE Id = ?", $id);
		$this->db->query($query);	
		return $this->db->result();
    }
    public function add_answer($id, $faq)
	{
       $name     = $faq->name;
       $email    = $faq->email;
       $qwestion = $faq->qwestion;
       $answer = $faq->answer;
       $orders = $faq->order;
       if (isset($id) && $id >0 && $answer != "")
       {
           $query = $this->db->placehold("UPDATE __faq SET name = ?, email = ?, qwestion = ?, answer = ?, status = ?, orders = ? WHERE Id = ?", $name, $email, $qwestion, $answer, 1, $orders,$id);
           
       }
       else
       {
           $query = $this->db->placehold("INSERT INTO __faq (name, email, qwestion, answer, status, orders) VALUES (? , ?, ?, ?, ?)", $name, $email, $qwestion, $answer, 1, $orders);
       }
       $this->db->query($query);
       return 0;

    }
    public function add_qwestion($sql)
	{

	    $name = $sql->name;
        $text = $sql->text;
        $email = $sql->email;
        $query = $this->db->placehold("INSERT INTO __faq (name, qwestion, email, status) VALUES (? , ?, ?, ?)", $name, $text, $email, 0);
        $this->db->query($query);
        return 0;
    }
    public function remove($id)
	{
	    if (isset($id) && $id >0)
        {
            
            $query = $this->db->placehold("DELETE FROM __faq WHERE Id = ?", $id);

            $this->db->query($query);
        }
        return 0;
    }
    
}