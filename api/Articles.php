<?php

/**
 * Simpla CMS
 *
 * @copyright	2011 Denis Pikusov
 * @link		http://simplacms.ru
 * @author		Denis Pikusov
 *
 */

require_once('Simpla.php');

class Articles extends Simpla
{

	/*
	*
	* Функция возвращает пост по его id или url
	* (в зависимости от типа аргумента, int - id, string - url)
	* @param $id id или url поста
	*
	*/
	public function get_article($id)
	{
		if(is_int($id))
			$where = $this->db->placehold(' WHERE b.id=? ', intval($id));
		else
			$where = $this->db->placehold(' WHERE b.url=? ', $id);
		
		$query = $this->db->placehold("SELECT b.id, b.category_id, b.url, b.name, b.annotation, b.text, b.meta_title,
		                               b.meta_keywords, b.meta_description, b.visible, b.date, b.position
		                               FROM __articles b $where LIMIT 1");
		if($this->db->query($query))
			return $this->db->result();
		else
			return false; 
	}
	
	/*
	*
	* Функция возвращает массив постов, удовлетворяющих фильтру
	* @param $filter
	*
	*/
	public function get_articles($filter = array())
	{	
		// По умолчанию
		$limit = 1000;
		$page = 1;
		$post_id_filter = '';
		$category_id_filter = '';
		$visible_filter = '';
		$keyword_filter = '';
		$order = 'b.position DESC';
		$posts = array();
		
		if(isset($filter['limit']))
			$limit = max(1, intval($filter['limit']));

		if(isset($filter['page']))
			$page = max(1, intval($filter['page']));

		if(!empty($filter['id']))
			$post_id_filter = $this->db->placehold('AND b.id in(?@)', (array)$filter['id']);
			
		if(!empty($filter['category_id']))
		{
			$category_id_filter = $this->db->placehold('AND b.category_id in(?@)', (array)$filter['category_id']);
		}			
			
		if(isset($filter['visible']))
			$visible_filter = $this->db->placehold('AND b.visible = ?', intval($filter['visible']));
      
      
 		if(!empty($filter['sort']))
			switch ($filter['sort'])
			{
				case 'position':
				$order = 'b.position DESC';
				break;
				case 'name':
				$order = 'b.name';
				break;
				case 'date':
				$order = 'b.date DESC,b.id DESC';
				break;

			}       		
		
		if(isset($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
				$keyword_filter .= $this->db->placehold('AND (b.name LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" OR b.meta_keywords LIKE "%'.mysql_real_escape_string(trim($keyword)).'%") ');
		}

		$sql_limit = $this->db->placehold(' LIMIT ?, ? ', ($page-1)*$limit, $limit);

		$query = $this->db->placehold("SELECT b.id,b.category_id, b.url, b.name, b.annotation, b.text,
		                                      b.meta_title, b.meta_keywords, b.meta_description, b.visible,
		                                      b.date, b.position
		                                      FROM __articles b WHERE 1 $post_id_filter $category_id_filter $visible_filter $keyword_filter
		                                      ORDER BY $order $sql_limit");
		
		$this->db->query($query);
		return $this->db->results();
	}
	
	
	/*
	*
	* Функция вычисляет количество постов, удовлетворяющих фильтру
	* @param $filter
	*
	*/
	public function count_articles($filter = array())
	{	
		$post_id_filter = '';
		$category_id_filter = '';
		$visible_filter = '';
		$keyword_filter = '';
		
		if(!empty($filter['id']))
			$post_id_filter = $this->db->placehold('AND b.id in(?@)', (array)$filter['id']);
			
		if(!empty($filter['category_id']))
		{
			$category_id_filter = $this->db->placehold('AND b.category_id in(?@)', (array)$filter['category_id']);
		}			
			
		if(isset($filter['visible']))
			$visible_filter = $this->db->placehold('AND b.visible = ?', intval($filter['visible']));     		

		if(isset($filter['keyword']))
		{
			$keywords = explode(' ', $filter['keyword']);
			foreach($keywords as $keyword)
				$keyword_filter .= $this->db->placehold('AND (b.name LIKE "%'.mysql_real_escape_string(trim($keyword)).'%" OR b.meta_keywords LIKE "%'.mysql_real_escape_string(trim($keyword)).'%") ');
		}
		
		$query = "SELECT COUNT(distinct b.id) as count
		          FROM __articles b WHERE 1 $post_id_filter $category_id_filter $visible_filter $keyword_filter";

		if($this->db->query($query))
			return $this->db->result('count');
		else
			return false;
	}
	
	/*
	*
	* Создание поста
	* @param $post
	*
	*/	
	public function add_article($post)
	{	
		if(isset($post->date))
		{
			$date = $post->date;
			unset($post->date);
			$date_query = $this->db->placehold(', date=STR_TO_DATE(?, ?)', $date, $this->settings->date_format);
		}
		$query = $this->db->placehold("INSERT INTO __articles SET ?% $date_query", $post);
		
		if(!$this->db->query($query))
			return false;
		else
			return $this->db->insert_id();
	}
	
	
	/*
	*
	* Обновить пост(ы)
	* @param $post
	*
	*/	
	public function update_article($id, $post)
	{
		$query = $this->db->placehold("UPDATE __articles SET ?% WHERE id in(?@) LIMIT ?", $post, (array)$id, count((array)$id));
		$this->db->query($query);
		return $id;
	}


	/*
	*
	* Удалить пост
	* @param $id
	*
	*/	
	public function delete_article($id)
	{
		if(!empty($id))
		{
			$query = $this->db->placehold("DELETE FROM __articles WHERE id=? LIMIT 1", intval($id));
			if($this->db->query($query))
			{
				$query = $this->db->placehold("DELETE FROM __comments WHERE type='article' AND object_id=? LIMIT 1", intval($id));
				if($this->db->query($query))
					return true;
			}
							
		}
		return false;
	}	
	

	/*
	*
	* Следующий пост
	* @param $post
	*
	*/	
	public function get_next_article($id)
	{
		$this->db->query("SELECT date FROM __articles WHERE id=? LIMIT 1", $id);
		$date = $this->db->result('date');
		$this->db->query("SELECT category_id FROM __articles WHERE id=? LIMIT 1", $id);
		$category_id = $this->db->result('category_id');		

		$this->db->query("(SELECT id FROM __articles WHERE date=? AND id>? AND visible  ORDER BY id limit 1)
		                   UNION
		                  (SELECT id FROM __articles WHERE date>? AND category_id=? AND visible ORDER BY date, id limit 1)",
		                  $date, $id, $date,$category_id);
		$next_id = $this->db->result('id');
		if($next_id)
			return $this->get_article(intval($next_id));
		else
			return false; 
	}
	
	/*
	*
	* Предыдущий пост
	* @param $post
	*
	*/	
	public function get_prev_article($id)
	{
		$this->db->query("SELECT date FROM __articles WHERE id=? LIMIT 1", $id);
		$date = $this->db->result('date');
		$this->db->query("SELECT category_id FROM __articles WHERE id=? LIMIT 1", $id);
		$category_id = $this->db->result('category_id');		

		$this->db->query("(SELECT id FROM __articles WHERE date=? AND id<? AND visible ORDER BY id DESC limit 1)
		                   UNION
		                  (SELECT id FROM __articles WHERE date<? AND category_id=? AND visible ORDER BY date DESC, id DESC limit 1)",
		                  $date, $id, $date, $category_id);
		$prev_id = $this->db->result('id');
		if($prev_id)
			return $this->get_article(intval($prev_id));
		else
			return false; 
	}
}
