<?php

/**
 * Simpla CMS
 *
 * @copyright	2015 Turusov Dima
 * @link		http://simplacms.ru
 * @author		Turusov Dima
 *
 */
 
require_once('Simpla.php');

class Revoke extends Simpla
{
	public function get_revoke($id_product) {
	   $query = $this->db->placehold("SELECT * FROM __revoke WHERE status = 1");
       $this->db->query($query);
	   return $this->db->results();	
	}
	

}