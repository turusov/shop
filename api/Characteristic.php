<?php

/**
 * Simpla CMS
 *
 * @copyright	2011 Denis Pikusov
 * @link		http://simplacms.ru
 * @author		Denis Pikusov
 *
 */
 
require_once('Simpla.php');

class Characteristic extends Simpla
{
    function check($name)
	{
        $query = $this->db->placehold('SELECT count(*) as count FROM __characteristics WHERE name = ?', $name);
        $this->db->query($query);
        $count = $this->db->results('count');
        return $count[0];
    }
    function add($elements)
    {
        $query = $this->db->placehold('INSERT INTO __characteristics SET name = ?', $elements->name);
        if ($this->db->query($query))
            return $this->db->insert_id();
        else
            return 0;
    }
    function add_var($id, $elements, $photo)
    {
        
        foreach ($elements as $key =>$element)
        {
             $query = $this->db->placehold('INSERT INTO __characteristics_value SET id_characteristics = ?, name = ?, img = ?',$id, $element, $photo[$key]);
             $this->db->query($query);
        }

    }
    function update_var($id, $elements, $photo)
    {
        $query = $this->db->placehold('SELECT Id FROM __characteristics_value WHERE id_characteristics = ? ORDER BY Id asc', $id);
        $this->db->query($query);   
        $id_char = $this->db->results();
        $k = 0;
        foreach ($elements as $key =>$element)
        {
             if (isset($id_char[$k]))
             {
                 if ($photo[$key-1])
                    $query = $this->db->placehold('UPDATE __characteristics_value SET name = ?, img = ? WHERE id_characteristics = ? AND Id = ?',$element, $photo[$key-1], $id, $id_char[$k]->Id);
                 else
                     $query = $this->db->placehold('UPDATE __characteristics_value SET name = ? WHERE id_characteristics = ? AND Id = ?',$element, $id, $id_char[$k]->Id);
                 $this->db->query($query);
                 $k ++;
             }
             else
             {
                 $query = $this->db->placehold('INSERT INTO __characteristics_value SET id_characteristics = ?, name = ?, img = ?',$id, $element, $photo[$key]);
                 $this->db->query($query);
             }
             
        }
    }
    function  remove_image($id) {
        $query = $this->db->placehold('UPDATE __characteristics_value SET img = "" WHERE Id = ?', $id);
        $this->db->query($query);
        return 1;
    }
    
    function update($elements)
    {
        $query = $this->db->placehold('UPDATE __characteristics SET name = ? WHERE Id = ?',$elements->name, $elements->id);
        if ($this->db->query($query))
            return 1;
        else
            return 0;
    }
    function select_all()
    {
        $query = $this->db->placehold('SELECT * FROM __characteristics ORDER BY `position` DESC');
        $this->db->query($query);
        return $this->db->results();
    }
    function position($position)
    {
        $postion = 1;
        foreach ($position as $key => $value)
        {
            $query = $this->db->placehold('UPDATE __characteristics SET position = ? WHERE Id = ', $postion, $key);
            $this->db->query($query);
            $postion++;
        }
    }
    function select($id)
    {
        $query = $this->db->placehold('SELECT * FROM __characteristics WHERE Id = ? ', $id);
        $this->db->query($query);
        return $this->db->results();
    }
    function select_value($id)
    {
        if ( is_numeric($id))
        {
            $query = $this->db->placehold('SELECT * FROM __characteristics_value WHERE id_characteristics = ? ORDER BY id asc', $id);
        }
        else
        {
            $query = $this->db->placehold('SELECT Id FROM __characteristics WHERE name = ? ', $id);
            $this->db->query($query);
            $id = $this->db->results('Id');
            $query = $this->db->placehold('SELECT * FROM __characteristics_value WHERE id_characteristics = ? ORDER BY id asc', $id[0]);
        }
        $this->db->query($query);
        return $this->db->results();
    }
    function remove ($id)
    {
        $query = $this->db->placehold('DELETE FROM __characteristics_value WHERE id_characteristics = ? ', $id);
        $this->db->query($query);
        $query = $this->db->placehold('DELETE FROM __characteristics WHERE Id = ? ', $id);
        $this->db->query($query);
        $query = $this->db->placehold('DELETE FROM __product_value WHERE id_characteristics = ? ', $id);
        $this->db->query($query);
    }
    function add_product($id_product, $params)
    {
        foreach ($params as $key=>$param)
        {
            $id_p_v = 0;
            foreach($param as $paramkey=> $element)
            {
                
                $id = "";
                $temp = $paramkey;
                $id_char = str_replace("add_" , "", $paramkey);
                if (str_replace("price_", "", $id_char))
                {
                    $id_char = str_replace("price_", "", $id_char);
                }
                $query = $this->db->placehold('SELECT id FROM __product_value WHERE id_product = ? AND id_characteristics_value = ? AND id_characteristics = ?', $id_product, $id_char, $key);
                if ($this->db->query($query))
                {
                   $id = $this->db->results(); 
                   $id = $id[0]->id; 
                }
                
                if ($id == "")
                {
                    $pos = strpos($paramkey, "add_price_");
                    
                    if ($pos === false) {
                         if ($element == "on")
                            $element = 1;
                         else
                            $element = 0;   
                         $query = $this->db->placehold('INSERT INTO __product_value SET id_product = ?, id_characteristics = ?, status = ?', $id_product, $key, $element);
                         $this->db->query($query);
                         $id_p_v = $this->db->insert_id(); 
                    } else {
                           $id_characteristics_value = str_replace ("add_price_", "", $paramkey);
                           if ($id_p_v != 0)
                           {
                               $query = $this->db->placehold('UPDATE __product_value SET add_price = ?, id_characteristics_value = ? WHERE id = ?', $element, $id_characteristics_value, $id_p_v);
                               $this->db->query($query);
                           }
                           else
                           {
                               $query = $this->db->placehold('INSERT INTO __product_value SET id_product = ?, id_characteristics = ?, add_price = ?, id_characteristics_value = ?, status = 0', $id_product, $key, $element, $id_characteristics_value);
                               $this->db->query($query);  
                           }
                           
                           $id_p_v = 0;
                    }   
                }
                else
                {          
                    echo $id;
                    exit;
                    if ($element != "on"){ 
                        if ($r != $id)
                            $query = $this->db->placehold('UPDATE __product_value SET add_price = ?, status = 0 WHERE id = ?', $element, $id);
                         else
                            $query = $this->db->placehold('UPDATE __product_value SET add_price = ? WHERE id = ?', $element, $id);
                         $this->db->query($query);
   
                    } else {
                        $query = $this->db->placehold('UPDATE __product_value SET id_product = ?, id_characteristics = ?, status = 1 WHERE id = ?', $id_product, $key, $id);
                         $this->db->query($query);
                        $r = $id;
                    }
                    //exit;
                }
            }
        }
    }
    function add_product_temp($id_product, $array) {
        foreach ($array as $id_characteristics=>$params) {
            foreach ($params as $id_param=>$param) {
                $query = $this->db->placehold('SELECT id FROM __product_value WHERE id_product = ? AND id_characteristics = ? AND id_characteristics_value = ?', $id_product, $id_characteristics, $id_param);
                $this->db->query($query);
                $array_param_id = $this->db->result('id');
                $status = 0;
                if ($param->add === "on")
                    $status = 1;
                if ($array_param_id) {
                    if ($param->add === "on")
                        $status = 1;
                    $query = $this->db->placehold('UPDATE __product_value SET add_price = ?, add_percent = ?, status = ? WHERE id = ?', $param->add_price, $param->add_percent, $status, $array_param_id);   
                } else {

                    $query = $this->db->placehold('INSERT INTO __product_value SET id_product = ?, id_characteristics = ?, add_price = ?, add_percent = ?, id_characteristics_value = ?, status = ?', $id_product, $id_characteristics, $param->add_price, $param->add_percent, $id_param, $status);
                }
                $this->db->query($query);
            }

        }
    }
    function select_products($id_product)
    {
        $query = $this->db->placehold('SELECT * FROM __product_value WHERE id_product = ?', $id_product);
        $this->db->query($query); 
        return $this->db->results(); 
    }
    function select_feth($id)
    {
        $query = $this->db->placehold('SELECT * FROM __product_value WHERE id_product = ? AND status = 1 ORDER BY id ASC', $id);
        $this->db->query($query);
        $temps = $this->db->results();

        foreach ($temps as $key=>$temp)
        {
            $array[$temp->id_characteristics][$temp->id_characteristics_value]['add_price'] = $temp->add_price;
            $array[$temp->id_characteristics][$temp->id_characteristics_value]['status'] = $temp->stat;
        }

     //   $array = (array_reverse($array));
                       
        
        
        
        foreach ($array as $key=>$arr)
        {
            foreach($arr as $key_par=>$value)
            {
                
                 $query = $this->db->placehold('SELECT name, img FROM __characteristics_value WHERE Id = ?', $key_par);
                 $this->db->query($query);  
                 $temp = $this->db->results();
                 $array[$key][$key_par]['name'] = $temp[0]->name;  
                  $array[$key][$key_par]['img'] = $temp[0]->img;            
            }
        }
        foreach ($array as $key=>$arr)
        {
             $query = $this->db->placehold('SELECT name FROM __characteristics WHERE Id = ?', $key);
             $this->db->query($query);  
             $title =  $this->db->results();
             $array[$key]['head'] = $title[0]->name;
        }
     
        
        
        return $array;
    }
}