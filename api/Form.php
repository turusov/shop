<?php

/**
 * Simpla CMS

 * @author		Turusov Dima
 *
 */

require_once('Simpla.php');

class Form extends Simpla
{
    public function add_dealer($dealer)
	{
	    $name = $dealer->name;
        $email = $dealer->email;
        $organization = $dealer->organization;
        $phone = $dealer->phone;
        $message = $dealer->message;
        $region = $dealer->region;
        $city = $dealer->city;
        $query = "INSERT INTO `__dealer`(`name`, `email`, `organization`, `phone`, `message`, `region`, `city`) VALUES ([value-1],[value-2])";
		$this->db->query($query);
		$results = $this->db->results();
        return 0;
    }
}